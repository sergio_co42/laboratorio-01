package com.curso.sesionbonus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Evento Onclick
        btnConfirmar.setOnClickListener {
            //Recuperar los valores
            val edad= edtEdad.text.toString().toInt()

            //Concatenar
            //val nEdad ="$edad"

            //Asignar el valor
            //tvResultado.text ="$nCompletos"
            if (edad < 18){
                tvResultado.text ="Es menor de edad"
            }else{
                tvResultado.text="Es Mayor de Edad"
            }
        }
    }
}